Project rationale:
======================================================================

clipski (forked from ClipIt) is a tray-resident GTK+ gui clipboard manager

Although clipit does provide a --with-gtk3 compile option,
its debian package has been orphaned, and debian has
chosen to "deprecate" clipit as of the debian 11(Bullseye) release.

Both clipit and parcellite tout their lightweighted-ness, yet both
have become fraught with longstanding and/or recurrent bugs related to
frilly (and proprietary, desktop environment -specific) features added along the way.
Appatanyaindicator thingie and guh-nome 'eggaccelerators'? Thanks, no thanks.

Parcellite is (as of Dec 2020) still slated for inclusion in Debian 11,
but it is 'GTK2-only', so will soon be be dropped from debian.
( ref: htt<b>p</b>s://sources.debian.org/src/parcellite/1.2.1-3/debian/control/ )

An alternative, Qt-based clipboard manager GUI utility (CopyQ), remains available
in Debian11... but (as tested on a 64bit system) its runtime memory footprint is 50MB+ !

Why use a "clipboard manager" ?
======================================================================

* copied text (and, optionally, any selected text) remains available for immediate pasting
   even if the program you are copying from closes / exits / crashes

* provides an option to Purge history after a configurable timeout
   (in case you are copypasting passwords)
<br>
background reading: <br>
 <br>
article titled "X Window selection" <br>
https://en.wikipedia.org/wiki/X_Window_selection <br>
 <br>
What is the difference between Primary Selection and Clipboard Buffer? <br>
https://unix.stackexchange.com/questions/139191/whats-the-difference-between-primary-selection-and-clipboard-buffer <br>
 <br>
https://askubuntu.com/questions/7769/keyboard-shortcut-for-pasting-the-primary-selection <br>
 <br>
https://wiki.archlinux.org/index.php/clipboard <br>
 <br>
((( for even more background reading, websearch "linux primary selection" )))

<br>

clipski features:
======================================================================
  * monitor Ctrl+C clipboard events and/or primary selection events
  * optionally, capture content of primary selection events into clipboard
  * optionally, maintain a history list of your copied snippets
  * optionally, purge history list and clipboard content after specified timeout
  * ability to edit and/or pin items (make permanent in history list)
  * as-you-type filtered history search
  * ability to selectively exclude from history
    (specified window names, strings, or regex)
  * optionally, trim whitespace from each newly added snippet


#### This project repository provides a debfile, packaged for use with antiX 19 (and debian buster repositories): <br>
 clipski_1.4.6_amd64.deb

________________________________________________<br><br>

How to DIY compile, create debfile, and install clipski (on an antiX or debian system)
======================================================================
 note: As of v1.4.6, clipski COMPILES AGAINST GTK3 ONLY (NOT GTK2 aka libgtk-2.*-dev)

#### manually download the clipski source code (see bottom of this README), or
<br> sudo apt install git
<br> mkdir -p /path/to/holdingpen # e.g. ~/Downloads/tmp
<br>
<br> cd /path/to/holdingpen
<br> git clone https://gitlab.com/skidoo/clipski
<br> cd clipski
<br> sudo apt install build-essential fakeroot debhelper intltool libgtk-3-dev
<br> dpkg-buildpackage -b
<br>sudo apt install /path/to/holdingpen/clipski_1.4.6_suffix.deb
<br># The exact debfile name, above, varies depending on your system architecture.
<br># Immediately afterward, to cleanup, you can optionally followup with:
<br># cd .. && rm -rf /path/to/holdingpen
<br>
<br>All set. To launch the program, type: clipski


links, credits:
======================================================================

clipski project website: https://gitlab.com/skidoo/clipski
 <br> <br>
ClipIt - Lightweight GTK+ Clipboard Manager
 <br>Copyright (C) 2010-2019 by Cristian Henzel
 <br>Copyright (C) 2011 by Eugene Nikolsky
 <br>Project website: https://github.com/CristianHenzel/ClipIt
 <br>older project page: http://sourceforge.net/projects/gtkclipit/

forked from parcellite, which is Copyright (C) 2007-2008 Gilberto 'Xyhthyx' Miralla
 <br>
The homepages of the forked prior art projects:
  <br>https://github.com/CristianHenzel/ClipIt
  <br>(older) http://sourceforge.net/projects/gtkclipit/
  <br>(defunct) http://clipit.rspwn.com
  <br>https://github.com/rickyrockrat/parcellite
  <br>(older) http://parcellite.sourceforge.net/


How to compile and install clipski (for non-debian systems)
===============================================================================

#### Requirements:
* libraries per the debian packages mentioned, above

#### Download the clipski source code, then:
    $ tar zxvf clipski-x.y.z.tar.gz
    $ cd clipski-x.y.z
    $ ./autogen.sh
    $ ./configure
    $ make
    $ sudo make install
