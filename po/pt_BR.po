# ckipski translation file.
# Copyright (C) 2010-2012 Cristian Henzel <email@redact.ed>
# This file is distributed under the same license as the ckipski package.
# 
# Translators:
# Phantom X <megaphantomx at bol.com.br>, 2008, 2009
# Alexandro Casanova <shorterfire@gmail.com>, 2012
msgid ""
msgstr ""
"Project-Id-Version: ckipski\n"
"Report-Msgid-Bugs-To: http://gitlab.com/skidoo/ckipski/issues\n"
"POT-Creation-Date: 2012-04-04 10:39+0300\n"
"PO-Revision-Date: 2013-04-28 09:09+0000\n"
"Last-Translator: Cristian Henzel <email@redact.ed>\n"
"Language-Team: Portuguese (Brazil) (http://www.transifex.com/projects/p/ckipski/language/pt_BR/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: pt_BR\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: ../data/ckipski-startup.desktop.in.h:1 ../data/ckipski.desktop.in.h:1
msgid "ckipski"
msgstr "ckipski"

#: ../data/ckipski-startup.desktop.in.h:2 ../data/ckipski.desktop.in.h:2
#: ../src/main.c:833
msgid "Clipboard Manager"
msgstr "Gerenciador de Área de Transferência"

#: ../src/main.c:287
msgid "Lightweight GTK+ clipboard manager."
msgstr "Gerenciador de Área de Transferência leve em GTK+."

#. Actions using:
#: ../src/main.c:356
msgid "Actions using:"
msgstr "Ações usando:"

#. Create menu item for empty clipboard contents
#: ../src/main.c:365 ../src/main.c:382
msgid "None"
msgstr "Nenhum"

#: ../src/main.c:385
msgid "<b>None</b>"
msgstr "<b>Nenhum</b>"

#. File contained no actions so adding empty
#. File did not open so adding empty
#. Nothing in history so adding empty
#: ../src/main.c:407 ../src/main.c:436 ../src/main.c:600
msgid "Empty"
msgstr "Vazio"

#. Edit actions
#: ../src/main.c:443
msgid "Edit actions"
msgstr "_Editar ações"

#. Standby mode checkbox
#: ../src/main.c:686
msgid "Standby mode"
msgstr "_Modo offiline"

#. Manage history
#: ../src/main.c:695
msgid "Manage history"
msgstr "_Gerenciar histórico"

#. Create the dialog
#: ../src/manage.c:138
msgid "Editing Clipboard"
msgstr "Editando a Área de Transferência"

#: ../src/manage.c:158
msgid "Static (pinned) item"
msgstr "_Item estático"

#: ../src/manage.c:227
msgid "Clear the history?"
msgstr "Limpar o histórico?"

#: ../src/manage.c:228
msgid "Clear history"
msgstr "Limpar histórico"

#: ../src/manage.c:348
msgid "Manage History"
msgstr "Gerenciar Histórico"

#: ../src/manage.c:351
msgid " (Standby mode)"
msgstr "(Modo standby)"

#: ../src/manage.c:393
msgid "Results"
msgstr "Resultados"

#: ../src/manage.c:398
msgid "Edit"
msgstr "Editar"

#: ../src/manage.c:400 ../src/preferences.c:886 ../src/preferences.c:947
msgid "Remove"
msgstr "Remover"

#: ../src/manage.c:402
msgid "Remove all"
msgstr "Remover tudo"

#: ../src/manage.c:404
msgid "Close"
msgstr "Fechar"

#: ../src/preferences.c:170
msgid "Save history"
msgstr "Salvar histórico"

#: ../src/preferences.c:203
msgid "Remove history file"
msgstr "Remover arquivo de histórico"

#. Create the dialog
#: ../src/preferences.c:648
msgid "clipski Preferences"
msgstr "Preferências"

#: ../src/preferences.c:667
msgid "Settings"
msgstr "Configurações"

#: ../src/preferences.c:675
msgid "<b>Clipboards</b>"
msgstr "<b>Áreas de Transferências</b>"

#: ../src/preferences.c:682
msgid "observe Copy (Ctrl+C) events"
msgstr "Usar _Cópia (Ctrl-C)"

#: ../src/preferences.c:685
msgid "observe Primary (aka primary selection) events"
msgstr "Usar Seleção _Primária"

#: ../src/preferences.c:688
msgid "Synchronize clipboards"
msgstr "S_incronizar áreas de transferência"

#: ../src/preferences.c:690
msgid "Automatically paste selected item"
msgstr "_Colar automaticamente item selecionado"

#: ../src/preferences.c:699
msgid "<b>Miscellaneous</b>"
msgstr "<b>Vários</b>"

#: ../src/preferences.c:706
msgid "Show indexes in history menu"
msgstr "Mostrar _índices no menu histórico"

#: ../src/preferences.c:708
msgid "Capture URIs (e.g. from filemanager copypase oerations)"
msgstr "S_alvar URIs"

#: ../src/preferences.c:712
msgid "Confirm before clearing history"
msgstr "C_onfirmar antes de limpar o histórico"

#: ../src/preferences.c:714
msgid "Use right-click menu (avoids MainMenu clutter)"
msgstr "_Use o botão direito do mouse no menu"

#: ../src/preferences.c:723
msgid "History"
msgstr "Histórico"

#: ../src/preferences.c:731
msgid "<b>History</b>"
msgstr "<b>Histórico</b>"

#: ../src/preferences.c:739
msgid "Save and restore history between sessions"
msgstr "Salvar e restaurar histórico entre as sessões"

#: ../src/preferences.c:743
msgid "Items in history:"
msgstr "Itens no histórico:"

#: ../src/preferences.c:752
msgid "Items in menu:"
msgstr "Itens do menu:"

#: ../src/preferences.c:759
msgid "Show static (pinned) items in menu"
msgstr "Mostrar _itens estáticos no menu"

#: ../src/preferences.c:764
msgid "Static items in menu:"
msgstr "Itens estáticos em menu:"

#: ../src/preferences.c:777
msgid "<b>Items</b>"
msgstr "<b>Itens</b>"

#: ../src/preferences.c:784
msgid "Show in a single line"
msgstr "Mostrar em uma unica _linha"

#: ../src/preferences.c:786
msgid "Show in reverse order"
msgstr "Exibir em ordem _reversa"

#: ../src/preferences.c:790
msgid "Character length of items:"
msgstr "Quantidade de caracteres dos itens:"

#: ../src/preferences.c:799
msgid "Omit items in the:"
msgstr "Omitir itens no:"

#: ../src/preferences.c:803
msgid "Beginning"
msgstr "Início"

#: ../src/preferences.c:804
msgid "Middle"
msgstr "Centro"

#: ../src/preferences.c:805
msgid "End"
msgstr "Fim"

#: ../src/preferences.c:835
msgid "Actions"
msgstr "Ações"

#. Build the actions label
#: ../src/preferences.c:840
msgid "Control-click ckipski's tray icon to use actions"
msgstr "Control-clique no ícone da bandeja do ckipski para usar ações"

#: ../src/preferences.c:860
msgid "Action"
msgstr "Ação"

#: ../src/preferences.c:867
msgid "Command"
msgstr "Comando"

#: ../src/preferences.c:882 ../src/preferences.c:943
msgid "Add..."
msgstr "Adicionar..."

#: ../src/preferences.c:903
msgid "Exclude"
msgstr "Excluir"

#. Build the exclude label
#: ../src/preferences.c:908
msgid ""
"Regex list of items that should not be inserted into the history "
"(passwords/sites that you don't need in history, etc)."
msgstr "Lista de expressões regulares de itens que não devem ser inseridas no histórico (senhas/sites que não necessita no histórico, etc)."

#: ../src/preferences.c:928
msgid "Regex"
msgstr "Expressões regulares"


#: ../src/utils.c:40 ../src/utils.c:47
#, c-format
msgid "Couldn't create directory: %s\n"
msgstr "Não foi possível criar diretório: %s\n"


#: ../src/utils.c:160
msgid ""
"Written by Cristian Henzel.\n"
"Report bugs to <email@redact.ed>."
msgstr "Escrito por Cristian Henzel.\nReportar bugs para <email@redact.ed>."
