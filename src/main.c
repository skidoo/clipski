/* Copyright (C) 2010-2012 by Cristian Henzel <email@redact.ed>
 * Copyright (C) 2011 by Eugene Nikolsky <email@redact.ed>
 *
 * forked from parcellite, which is
 * Copyright (C) 2007-2008 by Xyhthyx <email@redact.ed>
 *
 * This file is part of clipski.
 *
 * clipski is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * clipski is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#define _GNU_SOURCE

#include <glib.h>
#include <stdlib.h>
#include <gtk/gtk.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/keysym.h>   //  howdy    possibly no longer needed

#include "main.h"
#include "utils.h"
#include "history.h"
#include "preferences.h"
#include "manage.h"
#include "clipski-i18n.h"
#include <gdk/gdkkeysyms.h>    //  howdy    possibly no longer needed
#include <ctype.h>
#include <stdbool.h>
#include <string.h>

#include <sys/socket.h>
#include <sys/un.h>
#include <errno.h>


#define ICON "clipski-trayicon"
#define ICON_STANDBY "clipski-trayicon-standby"

static gchar* primary_text;
static gchar* clipboard_text;
static gchar* synchronized_text;
static GtkClipboard* primary;
static GtkClipboard* clipboard;

static GtkStatusIcon *status_icon;
static GtkWidget *statusicon_menu = NULL;
static gboolean status_menu_lock = FALSE;

static gboolean actions_lock = FALSE;

static Atom a_Primary = None;
static Atom a_Clipboard = None;
static Atom a_netWmName = None;
static Display *display = NULL;

prefs_t prefs = {DEF_USE_COPY,
                 DEF_USE_PRIMARY,
                 DEF_SYNCHRONIZE,
                 DEF_AUTOMATIC_PASTE,
                 DEF_SHOW_INDEXES,
                 DEF_TRIM_WHITESPACE,
                 DEF_SAVE_URIS,
                 DEF_USE_RMB_MENU,
                 DEF_SAVE_HISTORY,
                 DEF_HISTORY_LIMIT,
                 DEF_HISTORY_TIMEOUT,
                 DEF_HISTORY_TIMEOUT_SECONDS,
                 DEF_ITEMS_MENU,
                 DEF_STATICS_SHOW,
                 DEF_STATICS_ITEMS,
                 DEF_CONFIRM_CLEAR,
                 DEF_REVERSE_HISTORY,
                 DEF_ITEM_LENGTH,
                 DEF_ELLIPSIZE,
                 DEF_HYPERSONIC_POTATO,
                 DEF_STANDBY_MODE,
                 DEF_EXCLUDE_WINDOWS};    // howdy bub           tried literal NULL instead of name
//        currently unused (passes through, is ignored, has no effect):
/**              DEF_HYPERSONIC_POTATO,                */
// Currently moot    v--- b/c we ellipsize the content of any excessively long items
/**              DEF_SINGLE_LINE,                      */


//  variables for input buffer used for matching input to menu items
#define MAX_INPUT_BUF_SIZE 100
gchar input_buffer[MAX_INPUT_BUF_SIZE];
int input_index;


//  As the user types, attempt to match input with the values in the menu.
//  Only the first match will be activated.
gboolean selected_by_input(const GtkWidget *history_menu, const GdkEventKey *event);

//  Determine whether selection comes from excluded window
static gboolean is_selection_from_excluded_window(Atom selection_type) {
  gboolean excluded = FALSE;
  Window window;
  XTextProperty text_prop_return;
  gchar* window_name = NULL;
  int tmp;

  window = XGetSelectionOwner(display, selection_type);

  if (window != None)
  {
    // Using _NET_WM_NAME property instead of WM_NAME/XFetchName, because it is guaranted to be UTF-8 encoded.
    if (XGetTextProperty(display, window, &text_prop_return, a_netWmName))
    {
      window_name = (gchar*) text_prop_return.value;
    }
  }

  // XGetSelectionOwner doesn't work well for Qt apps. It returns unnamed window outside real app window hierarchy.
  // Try focused window instead.
  if (!window_name)
  {
    XGetInputFocus(display, &window, &tmp);

    if (XGetTextProperty(display, window, &text_prop_return, a_netWmName))
    {
      window_name = (gchar*) text_prop_return.value;
    }
  }

  if (window_name)
  {
    GRegex *regexp = NULL;
    if (prefs.exclude_windows && strlen(prefs.exclude_windows) > 0)
    {
      regexp = g_regex_new(prefs.exclude_windows, G_REGEX_CASELESS, 0, NULL);
      if (regexp) {
        excluded = g_regex_match(regexp, window_name, 0, NULL);
        g_regex_unref(regexp);
      }
    }
  }

  return excluded;
}

//  called every CHECK_INTERVAL seconds to check for new items
static gboolean item_check(gpointer data) {
  if (prefs.standby_mode)
    return TRUE;

  //  grab the current primary and clipboard text
  gchar* primary_temp = gtk_clipboard_wait_for_text(primary);
  gchar* clipboard_temp = gtk_clipboard_wait_for_text(clipboard);

  //  check if primary contents were lost
  if ((primary_temp == NULL) && (primary_text != NULL))
  {
    /* Check contents */
    gint count;
    GdkAtom *targets;
    gboolean contents = gtk_clipboard_wait_for_targets(primary, &targets, &count);
    g_free(targets);
    /* Only recover lost contents if there isn't any other type of content in the clipboard */
    if (!contents)
    {
      if(prefs.use_primary)
      {
        /* if use_primary is enabled, we restore from primary */
        gtk_clipboard_set_text(primary, primary_text, -1);
      }
      /* else
       * {
       *  // else, we restore from history
       *  GList* element = g_list_nth(history, 0);
       *  gtk_clipboard_set_text(primary, (gchar*)element->data, -1);
       * }
       */
    }
  }
  else
  {
    GdkModifierType button_state;
    GdkScreen *screen = gdk_screen_get_default();
    if (screen)
    {
      GdkDisplay *display = gdk_screen_get_display(screen);
      GdkWindow *window = gdk_screen_get_root_window(screen);
      GdkSeat *seat = gdk_display_get_default_seat(display);
      gdk_window_get_device_position(window, gdk_seat_get_pointer(seat), NULL, NULL, &button_state);
    }

    /* Proceed if mouse button not being held */
    if ((primary_temp != NULL) && !(button_state & GDK_BUTTON1_MASK) &&
             !is_selection_from_excluded_window(a_Primary))
    {
      //  check if primary is the same as the last entry
      if (g_strcmp0(primary_temp, primary_text) != 0)
      {
        //  new primary entry
        g_free(primary_text);
        primary_text = g_strdup(primary_temp);
        //  check if primary option is enabled and if there's text to add
        if (prefs.use_primary && primary_text) {
          check_and_append(primary_text);    // validate, match against exclusions...
        }
      }
    }
  }

  //  check if clipboard contents were lost
  if ((clipboard_temp == NULL) && (clipboard_text != NULL))
  {
    //  check contents
    gint count;
    GdkAtom *targets;
    gboolean contents = gtk_clipboard_wait_for_targets(clipboard, &targets, &count);
    g_free(targets);
    //  only recover lost contents if there isn't any other type of content in the clipboard
    if (!contents) {
///// g_print("Clipboard is null, recovering ...\n");     //  howdy
      gtk_clipboard_set_text(clipboard, clipboard_text, -1);
    }
  }
  else
  {
    //  check if clipboard is the same as the last entry
    if (g_strcmp0(clipboard_temp, clipboard_text) != 0 &&
      !is_selection_from_excluded_window(a_Clipboard))
    {
      //  new clipboard entry
      g_free(clipboard_text);
      clipboard_text = g_strdup(clipboard_temp);

	  //   howdy    trim whitespace     g_strchomp() removetrailing only    g_strchug() remove leading only
	  if (prefs.trim_whitespace) {
	      clipboard_text = g_strstrip(clipboard_text);
	  }

      //  check if clipboard option is enabled and if there's text to add
      if (prefs.use_copy && clipboard_text) {
        check_and_append(clipboard_text);
      }
    }
  }

  if (prefs.synchronize)
  {
    if (g_strcmp0(synchronized_text, primary_text) != 0)
    {
      g_free(synchronized_text);
      synchronized_text = g_strdup(primary_text);
      gtk_clipboard_set_text(clipboard, primary_text, -1);
    }
    else if (g_strcmp0(synchronized_text, clipboard_text) != 0)
    {
      g_free(synchronized_text);
      synchronized_text = g_strdup(clipboard_text);
      gtk_clipboard_set_text(primary, clipboard_text, -1);
    }
  }

  /* Cleanup */
  g_free(primary_temp);
  g_free(clipboard_temp);

  return TRUE;
}

//  called when execution action exits
static void action_exit(GPid pid, gint status, gpointer data) {
  g_spawn_close_pid(pid);
  actions_lock = FALSE;
}

/* Called when an action is selected from actions menu */
static void action_selected(GtkButton *button, gpointer user_data) {
  /* Enable lock */
  actions_lock = TRUE;

  /* Insert clipboard into command (user_data), and prepare it for execution */
  gchar* clipboard_text = gtk_clipboard_wait_for_text(clipboard);
  gchar* command = g_markup_printf_escaped((gchar*)user_data, clipboard_text);  // howdy  alternatively, g_shell_quote()
  g_free(clipboard_text);
  g_free(user_data);
  gchar* shell_command = g_shell_quote(command);
  g_free(command);
  gchar* cmd = g_strconcat("/bin/sh -c ", shell_command, NULL);
  g_free(shell_command);

  /* Execute action */
  GPid pid;
  gchar **argv;
  g_shell_parse_argv(cmd, NULL, &argv, NULL);   //   howdy   shell_escape to sanitize?
  g_free(cmd);
  g_spawn_async(NULL, argv, NULL, G_SPAWN_DO_NOT_REAP_CHILD, NULL, NULL, &pid, NULL);
  g_child_watch_add(pid, (GChildWatchFunc)action_exit, NULL);
  g_strfreev(argv);
}

//  called when Edit Actions is selected from actions menu   //   howdy     currently unused
static void edit_actions_selected(GtkButton *button, gpointer user_data) {
  //  Show the dialog on the actions tab
  show_preferences(ACTIONS_TAB);
}

//  called when an item is selected from history menu
static void item_selected(GtkMenuItem *menu_item, gpointer user_data) {
  //  Get the text from the right element and set as clipboard
  GList* element = g_list_nth(history, GPOINTER_TO_INT(user_data));
  history = g_list_remove_link(history, element);
  history = g_list_concat(element, history);
  history_item *elem_data = history->data;
  gtk_clipboard_set_text(clipboard, (gchar*)elem_data->content, -1);
  if (prefs.use_primary) {
      gtk_clipboard_set_text(primary, (gchar*)elem_data->content, -1);  // howdy
  }
  save_history();
  //  Paste the clipboard contents automatically if enabled
/**     as of v1.4.6, will only be enabled if user (knows and) manually edits =TRUE within .clipskirc       */
  if (prefs.automatic_paste) {
    gchar* cmd = g_strconcat("/bin/sh -c 'xdotool key ctrl+v'", NULL);   //   howdy   bash?  this detail should be user configurable?
    GPid pid;                                                            //   naw, not for this (unchanging) commandstring
    gchar **argv;
    g_shell_parse_argv(cmd, NULL, &argv, NULL);
    g_free(cmd);
    g_spawn_async(NULL, argv, NULL, G_SPAWN_DO_NOT_REAP_CHILD, NULL, NULL, &pid, NULL);
    g_child_watch_add(pid, (GChildWatchFunc)action_exit, NULL);
    g_strfreev(argv);
  }
}

/* Clears all local data (specific to main.c) */
void clear_main_data() {
  g_free(primary_text);
  g_free(clipboard_text);
  g_free(synchronized_text);
  primary_text = g_strdup("");
  clipboard_text = g_strdup("");
  synchronized_text = g_strdup("");
  gtk_clipboard_set_text(primary, "", -1);
  gtk_clipboard_set_text(clipboard, "", -1);
}



//  Called when Preferences is selected via menu
static void preferences_selected(GtkMenuItem *menu_item, gpointer user_data) {
  show_preferences(0);
}

//  called when Quit is selected via menu
static void quit_selected(GtkMenuItem *menu_item, gpointer user_data) {
  //  prevent quit while any dialogs are open
  if (!gtk_grab_get_current()) {
    gtk_main_quit();
  } else {
    //  a window is already open, so we present it to the user
    GtkWidget *toplevel = gtk_widget_get_toplevel(gtk_grab_get_current());
    gtk_window_present((GtkWindow*)toplevel);
  }
}

//  called when status icon is control-clicked    (currently unused, AcTiOnS functionality is now suppressed)
static gboolean show_actions_menu(gpointer data) {
  GtkWidget *menu, *menu_item, *item_label;

  menu = gtk_menu_new();
  g_signal_connect((GObject*)menu, "selection-done", (GCallback)gtk_widget_destroy, NULL);

  menu_item = gtk_menu_item_new_with_label(_("current snippet is:"));   //  howdy    "Run (execute) actions using:"
  g_signal_connect((GObject*)menu_item, "select", (GCallback)gtk_menu_item_deselect, NULL);
  gtk_menu_shell_append((GtkMenuShell*)menu, menu_item);

  gchar* text = gtk_clipboard_wait_for_text(clipboard);
  if (text != NULL)
  {
    menu_item = gtk_menu_item_new_with_label(_("None"));
    item_label = gtk_bin_get_child((GtkBin*)menu_item);
  //gtk_label_set_single_line_mode((GtkLabel*)item_label, TRUE);
    gtk_label_set_ellipsize((GtkLabel*)item_label, prefs.ellipsize);
    gtk_label_set_width_chars((GtkLabel*)item_label, 30);   // howdy
    gchar* bold_text = g_markup_printf_escaped("<b>%s</b>", text);
    gtk_label_set_markup((GtkLabel*)item_label, bold_text);
    g_free(bold_text);
    g_signal_connect((GObject*)menu_item, "select", (GCallback)gtk_menu_item_deselect, NULL);
    gtk_menu_shell_append((GtkMenuShell*)menu, menu_item);
  }
  else
  {
    //  create menu item for empty clipboard contents
    menu_item = gtk_menu_item_new_with_label(_("None"));
    /* Modify menu item label properties */
    item_label = gtk_bin_get_child((GtkBin*)menu_item);
    gtk_label_set_markup((GtkLabel*)item_label, _("<b>None</b>"));
    g_signal_connect((GObject*)menu_item, "select", (GCallback)gtk_menu_item_deselect, NULL);
    gtk_menu_shell_append((GtkMenuShell*)menu, menu_item);
  }
  /* -------------------- */
  gtk_menu_shell_append((GtkMenuShell*)menu, gtk_separator_menu_item_new());

  menu_item = gtk_menu_item_new_with_label(_("available Actions:"));
  g_signal_connect((GObject*)menu_item, "select", (GCallback)gtk_menu_item_deselect, NULL);
  gtk_menu_shell_append((GtkMenuShell*)menu, menu_item);

  /* Actions */
  gchar* path = g_build_filename(g_get_user_data_dir(), ACTIONS_FILE, NULL);
  FILE* actions_file = fopen(path, "rb");
  g_free(path);
  /* Check that it opened and begin read */
  if (actions_file)
  {
    gint size;
    size_t fread_return;
    fread_return = fread(&size, 4, 1, actions_file);

//      v---- THIS WAS INCORRECTLY DISPLAYED, EVEN WHEN 1+ ACTIONS ARE AVAILABLE      howdy
    //  check if actions file is empty
    if (!size)
    {
      //  File contained no actions so adding empty
      menu_item = gtk_menu_item_new_with_label(_("the list is currently empty"));
      gtk_widget_set_sensitive(menu_item, FALSE);
      gtk_menu_shell_append((GtkMenuShell*)menu, menu_item);
    }

    //  Continue reading items until size is 0
    while (size)
    { //  Read action name
      gchar* name = (gchar*)g_malloc(size + 1);
      fread_return = fread(name, size, 1, actions_file);
      name[size] = '\0';
      menu_item = gtk_menu_item_new_with_label(name);
      g_free(name);
      fread_return = fread(&size, 4, 1, actions_file);
      //  Read action commandstring
      gchar* command = (gchar*)g_malloc(size + 1);
      fread_return = fread(command, size, 1, actions_file);
      command[size] = '\0';
      fread_return = fread(&size, 4, 1, actions_file);
      //  Append the action
      gtk_menu_shell_append((GtkMenuShell*)menu, menu_item);
      g_signal_connect((GObject*)menu_item, "activate", (GCallback)action_selected, (gpointer)command);
      g_free(command);    // late addition
    }
    fclose(actions_file);
  }
  else
  { //  File did not open so adding empty
    menu_item = gtk_menu_item_new_with_label(_("Empty"));
    gtk_widget_set_sensitive(menu_item, FALSE);
    gtk_menu_shell_append((GtkMenuShell*)menu, menu_item);
  }
  /* -------------------- */
  gtk_menu_shell_append((GtkMenuShell*)menu, gtk_separator_menu_item_new());

//  howdy    placeholder      maybe inject a context-specific "Help: actions" link
/**
  menu_item = gtk_menu_item_new_with_label(_("Remove all actions"));
  gtk_menu_shell_append((GtkMenuShell*)menu, menu_item);
  g_signal_connect((GObject*)menu_item, "activate", (GCallback)bogus_we_have_no_such_fn, NULL);
*/
  menu_item = gtk_menu_item_new_with_label(_("Edit actions"));
  g_signal_connect((GObject*)menu_item, "activate", (GCallback)edit_actions_selected, NULL);
  gtk_menu_shell_append((GtkMenuShell*)menu, menu_item);
  //  Popup the menu...
  gtk_widget_show_all(menu);
  gtk_menu_popup((GtkMenu*)menu, NULL, NULL, NULL, NULL, 1, gtk_get_current_event_time());
  //  Return false so the g_timeout_add() function is called only once
  return FALSE;
}

//  Append a character to the input buffer
static void append_to_input_buffer(gchar *character) {
  if (input_index < MAX_INPUT_BUF_SIZE) {
    input_buffer[input_index++] = *character;
    input_buffer[input_index] = '\0';
  }
}

//  Remove the last character from the input buffer
static void remove_from_input_buffer() {
  if (input_index > 0) {
    input_buffer[--input_index] = '\0';
  }
}

//  truncate the input buffer (called each time the menu is destroyed)
static void clear_input_buffer() {
  input_index = 0;
  input_buffer[input_index] = '\0';
}

//  Handle user input while the menu is open
static gboolean menu_key_pressed(GtkWidget *history_menu, GdkEventKey *event, gpointer user_data) {
  //   howdy   conditional b/c the code previously referenced the now-nixed "selected_by_digit" handler here
  //if (!handled) {

  gboolean handled;
  handled = selected_by_input(history_menu, event);
  //}
  return handled;
}


/* Draw an underline under the matched portion of text.
 * match should be a pointer to the first character of the matched text.
 */
void underline_match(char* match, GtkMenuItem* menu_item, const gchar* menu_label) {
  if (!match)
    return;

  int start = match - menu_label;
  int end = start + strlen(input_buffer);

  PangoAttribute* underline = pango_attr_underline_new (PANGO_UNDERLINE_SINGLE);
  underline->start_index = start;
  underline->end_index = end;

  PangoAttrList* attr_list = pango_attr_list_new();
  pango_attr_list_insert (attr_list, underline);

  GtkWidget* gtk_label = gtk_bin_get_child (GTK_BIN (menu_item));
  gtk_label_set_attributes(GTK_LABEL(gtk_label), attr_list);
}


//  As the user types, attempt to match input with the values in the menu.
//  The matched text will be underlined and non-matching entries greyed out.
gboolean selected_by_input(const GtkWidget *history_menu, const GdkEventKey *event) {
  //    tested:  no effect in mainmenu. Delete is effective in Manage dialog (backspace, for my kb, is ignored)
  //              ...so, not mentioning these in helpdoc
  if (event->keyval == GDK_KEY_Delete || event->keyval == GDK_KEY_BackSpace) {
    remove_from_input_buffer();
  } else if (event->keyval == GDK_KEY_KP_Enter || event->keyval == GDK_KEY_Return || event->keyval == GDK_KEY_Escape) {
    clear_input_buffer();
    return FALSE;
  }

  if (event->keyval == GDK_KEY_Down || event->keyval == GDK_KEY_Up) {
    return FALSE;
  }

  if (isprint(*event->string))
    append_to_input_buffer(event->string);

  GtkMenuShell* menu = (GtkMenuShell *) history_menu;
  GList* element = gtk_container_get_children(menu);
  GtkMenuItem *menu_item, *first_match = 0;

  const gchar* menu_label;
  int count = 0, match_count = 0;
  char* match;

  while (element->next != NULL && count < prefs.items_menu) {
    menu_item = (GtkMenuItem *) element->data;
    menu_label = gtk_menu_item_get_label(menu_item);

    gtk_menu_item_deselect(menu_item);

    match = strcasestr(menu_label, input_buffer);
    if (match) {
      if (!first_match)
        first_match = menu_item;
      match_count++;
      underline_match(match, menu_item, menu_label);
      gtk_widget_set_sensitive(menu_item, true);
    } else {
      gtk_widget_set_sensitive(menu_item, false);
    }
    element = element->next;
    count++;
  }

  if (first_match && match_count != prefs.items_menu) {
    gtk_menu_item_select(first_match);
	gtk_menu_shell_select_item(menu, first_match);
    return TRUE;
  }
  return FALSE;
}


static void toggle_standby_mode() {
	if (prefs.standby_mode) {
		//  Restore clipboard contents before turning standby mode off
		gtk_clipboard_set_text(clipboard, clipboard_text != NULL ? clipboard_text : "", -1);
	}

	prefs.standby_mode = !prefs.standby_mode;
    gtk_status_icon_set_from_icon_name(status_icon, prefs.standby_mode ? ICON_STANDBY : ICON);
	save_preferences();
}

//  upon menu closure, clear the input buffer
gboolean menu_destroyed(const GtkWidget *history_menu, const GdkEventKey *event) {
  clear_input_buffer();
  return FALSE;
}

static GtkWidget *create_history_menu(GtkWidget *history_menu) {
	GtkWidget *menu_item, *item_label;
	history_menu = gtk_menu_new();
	g_signal_connect((GObject*)history_menu, "key-press-event", (GCallback)menu_key_pressed, NULL);
	g_signal_connect((GObject*)history_menu, "destroy", (GCallback)menu_destroyed, NULL);

	if ((history != NULL) && (history->data != NULL))
	{
		GList* element;
		gint element_number = 0;
		gint element_number_small = 0;
		gchar* primary_temp = gtk_clipboard_wait_for_text(primary);
		gchar* clipboard_temp = gtk_clipboard_wait_for_text(clipboard);

		if (prefs.reverse_history) {
			history = g_list_reverse(history);
			element_number = g_list_length(history) - 1;
		}

		for (element = history; (element != NULL) && (element_number_small < prefs.items_menu); element = element->next)
		{
			history_item *elem_data = element->data;
			GString* string = g_string_new((gchar*)elem_data->content);

			string = ellipsize_string(string);
			/* Remove control characters */            // should be conditionally, if prefs.singleline=true ?
			string = remove_newlines_string(string);   // why now?   (too late to sanitize)

			//  make new item, using ellipsized text
			gchar* list_item;
			if (prefs.show_indexes) {
				list_item = g_strdup_printf("%d. %s", (element_number_small+1), string->str);
			} else {
				list_item = g_strdup(string->str);
			}
			menu_item = gtk_menu_item_new_with_label(list_item);
			g_signal_connect((GObject*)menu_item, "activate", (GCallback)item_selected, GINT_TO_POINTER(element_number));

			//  modify menu item label properties
			item_label = gtk_bin_get_child((GtkBin*)menu_item);
          //gtk_label_set_single_line_mode((GtkLabel*)item_label, prefs.single_line);

			//  if item is also the current clipboard text, make bold
			if ((clipboard_temp) && (g_strcmp0((gchar*)elem_data->content, clipboard_temp) == 0))
			{
				gchar* bold_text = g_markup_printf_escaped("<b>%s</b>", list_item);
				gtk_label_set_markup((GtkLabel*)item_label, bold_text);
				g_free(bold_text);
			}
			else if ((primary_temp) && (g_strcmp0((gchar*)elem_data->content, primary_temp) == 0))
			{
				gchar* italic_text = g_markup_printf_escaped("<i>%s</i>", list_item);
				gtk_label_set_markup((GtkLabel*)item_label, italic_text);
				g_free(italic_text);
			}
			g_free(list_item);

			gtk_menu_shell_append((GtkMenuShell*)history_menu, menu_item);
			g_string_free(string, TRUE);

			if (prefs.reverse_history)
				element_number--;
			else
				element_number++;
			element_number_small++;
		}
		g_free(primary_temp);
		g_free(clipboard_temp);
		//   restore history to normal if reversed
		if (prefs.reverse_history)
			history = g_list_reverse(history);
	}
	else
	{
		// Nothing in history so adding empty    howdy   for mainmenu, we do not grab the current clipboard?
		menu_item = gtk_menu_item_new_with_label(_("^--- the history list is currently empty"));  // howdy bub
		gtk_widget_set_sensitive(menu_item, FALSE);
		gtk_menu_shell_append((GtkMenuShell*)history_menu, menu_item);
	}
	if (prefs.statics_show) {
		/* -------------------- */
		gtk_menu_shell_append((GtkMenuShell*)history_menu, gtk_separator_menu_item_new());

		//  Items
		GList *elem = history;
		int elem_num = 0;
		int elem_num_static = 1;
		while (elem && (elem_num_static <= prefs.statics_items)) {
			history_item *hitem = elem->data;
			if (hitem->is_static) {
				GString* string = g_string_new((gchar*)hitem->content);
				string = ellipsize_string(string);
				string = remove_newlines_string(string);
				gchar* list_item;
				if (prefs.show_indexes)
				{
					list_item = g_strdup_printf("%d. %s", (elem_num_static), string->str);
				} else {
					list_item = g_strdup(string->str);
				}
				menu_item = gtk_menu_item_new_with_label(list_item);
				g_signal_connect((GObject*)menu_item, "activate", (GCallback)item_selected, GINT_TO_POINTER(elem_num));
				// Modify menu item label properties
				item_label = gtk_bin_get_child((GtkBin*)menu_item);
                //gtk_label_set_single_line_mode((GtkLabel*)item_label, prefs.single_line);
				g_free(list_item);
				g_string_free(string, TRUE);
				gtk_menu_shell_append((GtkMenuShell*)history_menu, menu_item);
				elem_num_static++;
			}
			elem_num++;
			elem = elem->next;
		}
	}

	if (prefs.standby_mode) {
		gtk_menu_shell_append((GtkMenuShell*)history_menu, gtk_separator_menu_item_new());

		menu_item = gtk_menu_item_new_with_label("");
		item_label = gtk_bin_get_child((GtkBin*)menu_item);
		gtk_label_set_markup((GtkLabel*)item_label, "<b>Standby mode is active</b> [history addition suspended]");
      //gtk_label_set_single_line_mode((GtkLabel*)item_label, TRUE);
		gtk_widget_set_sensitive(item_label, FALSE);
		gtk_menu_shell_append((GtkMenuShell*)history_menu, menu_item);
	}
	return history_menu;
}


//void foff(GtkWindow *helpwindow) {    gtk_widget_destroy(GtkWidget *helpwindow); }


void show_help_window(GtkMenuItem *menu_item, gpointer user_data)
{
    GtkWindow *helpwindow = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title( helpwindow, _("clipski Help and usage guide") );
    gtk_window_set_default_size(GTK_WINDOW(helpwindow), 780, 580);
    gtk_window_set_resizable(GTK_WINDOW(helpwindow), TRUE);
    gtk_container_set_border_width(GTK_CONTAINER(helpwindow), 6);
    gtk_window_set_deletable(helpwindow, TRUE);  // howdy  implicitly true. late addition attempt to gen Close button
    gtk_window_set_modal(helpwindow, TRUE); //   howdy   laaaaate addition

    GtkBox *topvbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_container_add(GTK_CONTAINER(helpwindow), topvbox);

    GtkWidget* scrolled_window_help = gtk_scrolled_window_new(
             (GtkAdjustment*)gtk_adjustment_new(0, 0, 0, 0, 0, 0),
             (GtkAdjustment*)gtk_adjustment_new(0, 0, 0, 0, 0, 0));
    gtk_scrolled_window_set_policy((GtkScrolledWindow*)scrolled_window_help, GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
    gtk_scrolled_window_set_shadow_type((GtkScrolledWindow*)scrolled_window_help, GTK_SHADOW_NONE);
    gtk_container_set_border_width(GTK_CONTAINER (scrolled_window_help), 4);
    gtk_scrolled_window_set_min_content_width(scrolled_window_help, 600);
    gtk_box_pack_start(topvbox, scrolled_window_help, TRUE, TRUE, 6);    // last arg is padding

    ////  Using a 'full-blown' textview to achieve selectable text is counterproductive
    ////  both memory-wise and because the user may become confused upon discovering
    ////  that s/he cannot SAVE, in place, the edited text.
    GtkLabel *label = gtk_label_new(NULL);
    gtk_label_set_line_wrap(label, TRUE);
    gtk_label_set_selectable(label, TRUE);   ///////      whoa!  it is opening SelectAll PRE-SELECTED

    //      https://developer.gnome.org/pango/stable/pango-Markup.html

    //     grrrrrrrrrrrrrrrrrr       theme applies a disappearing scrollbar
    gtk_label_set_markup (label,
"<span foreground='blue'><b>clipski v1.4.6 help and usage tips</b></span>\n \
\n \
<b>Why use a 'clipboard manager' ?</b>\n \
\n \
-- copied text (and, optionally, any selected text) remains available for immediate pasting\n \
   even if the program you are copying from closes / exits / crashes\n \
\n \
-- provides an option to Purge history after a configurable timeout\n \
   (in case you are copypasting passwords)\n \
\n \
background reading:\n \
        you can drag-select to copy these URLs, for pasting to web browser addressbar\n \
\n \
article titled 'X Window selection'\n \
https://en.wikipedia.org/wiki/X_Window_selection\n \
      -=- \n \
What is the difference between Primary Selection and Clipboard Buffer?\n \
https://unix.stackexchange.com/questions/139191/whats-the-difference-between-primary-selection-and-clipboard-buffer\n \
      -=- \n \
https://askubuntu.com/questions/7769/keyboard-shortcut-for-pasting-the-primary-selection\n \
      -=- \n \
https://wiki.archlinux.org/index.php/clipboard\n \
      \n \
       (((  for further background reading, websearch 'linux primary selection'  )))\n \
\n \
===============================================================================\n \
\n \
* When history is enabled,  clicking an item in the main menu will:\n \
     -- push that item's content into the clipboard, ready for pasting\n \
     -- float that item to top position in the history list\n \
     -- close the mainmenu\n \
\n \
* in the Manage History window, DOUBLE-clicking an item will:\n \
     -- push that item's content into the clipboard, ready for pasting\n \
     -- float that item to top position in the history list\n \
     -- close the Manage History window\n \
\n \
<b>TIP:</b> consider prepending  GTK_THEME=<i>some-theme-name</i> in the commandline when launching clipski\n \
         (apply a dark theme, or a preferred colorscheme, different from other day-to-say apps)\n \
\n \
===============================================================================\n \
\n \
Q. Can I run clipski concurrently, along with ClipIt or CopyQ or other clipboard manager?\n \
        -=- \n \
A. Yes (non-fatal) but doing so would be silly (redundant).\n \
\n \
Q. What is the rationale for having a STANDBY (aka suspend) mode?\n \
        -=- \n \
A. Security, if you are copying and pasting passwords.\n \
    While in standby mode, clipski does not save the password in history,\n \
    just in RAM (avoids having the password in clipit's plaintext history file).\n \
\n \
Q. Does the option 'Purge history after XX seconds' also blank the current clipboard content?\n \
        -=- \n \
A. (tested) Yes, it does. Perhaps that behavior should be activated via a <u>separate</u> option...\n \
\n \
Q. Why, when launching clipski from commandline, might I see an errormessage:\n \
       Theme parsing error: gtk-w. . .\n \
        -=- \n \
A. This msg is a byproduct of your currently selected GTK+ desktop theme.\n \
    (It does NOT appear if the active GTK desktop theme DOES declare units within its css)\n \
\n \
Tip: a greyed icon indicates 'standby mode' (not 'clipboard history is empty')\n \
\n \
Q. Depending on your display resolution and the fontsize of your current desktop theme,\n \
    the clipski popup-from-tray window may be too tall to fit your screen. If so, you can\n \
        -=- \n \
A. edit your ~/.config/clipski file, specify a lower value for the 'items_menu' (default is 24)\n \
\n \
Q. Why do I see strange blocks (tofu) instead of regular characters in some of my history items?\n \
        -=- \n \
A. This occurs when your selected desktop font lacks glyphs for certain unicode characters.\n \
    You can eliminate this by (installing and?) selecting a font which provides\n \
    more complete coverage of unicode characters.\n \
\n \
Q. First-run mentions saving history in a plaintext file, but the history file seems to be\n \
    unreadable via my text editor. How can I view (and/or export) the history file content?\n \
        -=- \n \
A. Yes, the saved history content exists as plaintext strings, but some unprintable characters\n \
    likely exist within the history file. To view/extract, this should suit:\n \
       /usr/bin/strings ~/.local/share/clipski/history >> /path/to/a_file.txt\n \
\n \
Q. When I have the Manage window (kept) open, why are newly-added snippets are not shown?\n \
        -=- \n \
A. Window auto-refresh would disturb the index of your currently selected item\n \
    (which you may be editing when the refresh occurs). Note that the window content\n \
    DOES refresh with each as-you-type-search keystroke, ensuring accurate search results.\n \
\n \
Q. Hey, forked from ClipIt... so, where is the autopaste feature?\n \
        -=- \n \
A. The UI tickbox to enable this arguably mishap-prone feature (e.g. ass-identally\n \
    pasting password into browser addressbar, to be harvested by searchenginepartner)\n \
    has been omitted. A saavy user will have read this README, will understand the\n \
    potential risk, will understand that use of autopaste requires presence of\n \
    'xdotool' command (provided by debian package 'binutils') installed on the system,\n \
    and will manually edit ~/.config/clipski/clipskirc to specify: automatic_paste=true\n \
\n \
Q. (not a question) consider: if you configure 'max 1000 history entries' and\n \
    'maxsize per entry 1MB'... a gigabyte of data could wind up accumulating in the\n \
    clipboard history file !\n \
        -=- \n \
A. For this reason (bloat, not privacy) consider emptying the history prior to\n \
    performing an antiX persist-save or live-remaster or iso-snapshot operation.\n \
    (Instead of trying to remember this step, you might instead create an entry\n \
     within the /usr/local/share/exclude/*.list files)\n \
\n \
Q. (not a question) One notable side effect -- not a quirk, not a bug -- from enabling\n \
    the optional 'monitor Primary selection': each time a window containing a span of\n \
    drag-selected text (or text previously-selected via a SelectAll command) gains focus,\n \
    a fresh X11 'selection event' is emitted. This native behavior is not altered by\n \
    clipski and becomes (perhaps surprisingly) notable if you opt to have clipski\n \
    monitor the primarySelection events and/or synchronize (capture into clipboard,\n \
    and add to history) content which has been emitted by each selection event.\n \
        -=- \n \
    'monitoring primary, plus synchronizing' is helpful during certain workflows,\n \
    but can be a nuisance in other workflows. (In other words, you will probably not\n \
    wish to leave sync enabled, permanently, throughout your day-to-day computing.");
    gtk_container_add(GTK_CONTAINER(scrolled_window_help), label);

    GtkWidget *byebutton;
    byebutton = gtk_button_new_with_label(_("Close"));
    //g_signal_connect(byebutton, "clicked", (GCallback)foff, &helpwindow );
    g_signal_connect_swapped (byebutton, "clicked", G_CALLBACK (gtk_widget_destroy), helpwindow);

    gtk_container_add(GTK_CONTAINER(topvbox), byebutton);

    gtk_widget_show_all(GTK_WINDOW(helpwindow) );
  //gtk_window_set_position(GTK_WINDOW(helpwindow), GTK_WIN_POS_CENTER);
    // howdy  grrrr      centering  call to set_position() here was ignored
    gtk_label_select_region(label, -1, -1);  //  howdy    must call this AFTER the object has be realized
}






static GtkWidget *create_tray_menu(GtkWidget *tray_menu, int menu_type) {
	GtkWidget *menu_item; //, *menu_image;    // late removal

	if ((menu_type == 1) || (menu_type == 2)) {
		tray_menu = create_history_menu(tray_menu);
	} else {
		tray_menu = gtk_menu_new();
	}
	if (!prefs.use_rmb_menu || (menu_type == 2)) {
		/* -------------------- */
		gtk_menu_shell_append((GtkMenuShell*)tray_menu, gtk_separator_menu_item_new());
	}
	/* We show the options only if:
	 * - use_rmb_menu is active and menu_type is right-click, OR
	 * - use_rmb_menu is inactive and menu_type is left-click */
	if ((prefs.use_rmb_menu && (menu_type == 3)) || (!prefs.use_rmb_menu) || (menu_type == 2))
    {
		menu_item = gtk_check_menu_item_new_with_label(_("Standby mode"));  // howdy    verbiage
		gtk_check_menu_item_set_active((GtkCheckMenuItem*)menu_item, prefs.standby_mode);
		g_signal_connect((GObject*)menu_item, "activate", (GCallback)toggle_standby_mode, NULL);
		gtk_menu_shell_append((GtkMenuShell*)tray_menu, menu_item);

		menu_item = gtk_menu_item_new_with_label(_("Manage history"));
		g_signal_connect((GObject*)menu_item, "activate", (GCallback)show_search_dlg, NULL);
		gtk_menu_shell_append((GtkMenuShell*)tray_menu, menu_item);

		menu_item = gtk_menu_item_new_with_label(_("Preferences"));
		g_signal_connect((GObject*)menu_item, "activate", (GCallback)preferences_selected, NULL);
		gtk_menu_shell_append((GtkMenuShell*)tray_menu, menu_item);
//   howdy         suppressing this indefinitely
/**
		menu_item = gtk_menu_item_new_with_label(_("Actions"));
		g_signal_connect((GObject*)menu_item, "activate", (GCallback)dodirect_actions_menu, NULL);
		gtk_menu_shell_append((GtkMenuShell*)tray_menu, menu_item);
*/
		menu_item = gtk_menu_item_new_with_label(_("Help"));
		g_signal_connect((GObject*)menu_item, "activate", (GCallback)show_help_window, NULL);
		gtk_menu_shell_append((GtkMenuShell*)tray_menu, menu_item);

		menu_item = gtk_menu_item_new_with_label(_("Quit"));
		g_signal_connect((GObject*)menu_item, "activate", (GCallback)quit_selected, NULL);
		gtk_menu_shell_append((GtkMenuShell*)tray_menu, menu_item);
	}
	//  popup the menu...
	gtk_widget_show_all(tray_menu);
	return tray_menu;
}


//  called when status icon is clicked
static void show_clipski_menu(int menu_type) {
	//  If the menu is already visible, we don't do anything, so that it gets hidden
	if((statusicon_menu != NULL) && gtk_widget_get_visible((GtkWidget *)statusicon_menu))
		return;
	if(status_menu_lock)
		return;
	status_menu_lock = TRUE;
	//  create the menu
	statusicon_menu = create_tray_menu(statusicon_menu, menu_type);
	g_signal_connect((GObject*)statusicon_menu, "selection-done", (GCallback)gtk_widget_destroy, NULL);

	gtk_widget_set_visible(statusicon_menu, TRUE);
	gtk_menu_popup((GtkMenu*)statusicon_menu, NULL, NULL, gtk_status_icon_position_menu, status_icon, 1, gtk_get_current_event_time());
	gtk_menu_shell_select_first((GtkMenuShell*)statusicon_menu, TRUE);
	status_menu_lock = FALSE;
}

//   Called when status icon is clicked
//     (checks type of click and calls correct function)
static void status_icon_clicked(GtkStatusIcon *status_icon, GdkEventButton *event ) {
	GdkModifierType state;
	gtk_get_current_event_state(&state);  //  Check what type of click was recieved
	//  Control (Ctrl) click
	if (state == GDK_MOD2_MASK+GDK_CONTROL_MASK || state == GDK_CONTROL_MASK) {
		if (actions_lock == FALSE) {
//           howdy     suppressing this indefinitely
/**
			g_timeout_add(POPUP_DELAY, show_actions_menu, NULL);
*/
		}
	}
	//  Left click
	else if (event->button == 1) {
		show_clipski_menu(1);
	}
	else if (event->button == 3) {
		if (prefs.use_rmb_menu)
			show_clipski_menu(3);
	}
}


//  Startup calls and initialization
static void clipski_init() {
	//  create clipboard
	primary = gtk_clipboard_get(GDK_SELECTION_PRIMARY);
	clipboard = gtk_clipboard_get(GDK_SELECTION_CLIPBOARD);
	g_signal_connect(primary, "owner-change", G_CALLBACK(item_check), NULL);
	g_signal_connect(clipboard, "owner-change", G_CALLBACK(item_check), NULL);

	display = XOpenDisplay(NULL);
	a_Primary = XInternAtom(display, "PRIMARY", True);
	a_Clipboard = XInternAtom(display, "CLIPBOARD", True);
	a_netWmName = XInternAtom(display, "_NET_WM_NAME", True);

	read_preferences();
	if (prefs.save_history)   read_history();

	status_icon = gtk_status_icon_new_from_icon_name(ICON);
	g_signal_connect((GObject*)status_icon, "button_press_event", (GCallback)status_icon_clicked, NULL);
/**
////   status icons depend on the existence of a notification area being available to the user;
////    you should not use status icons as the only way to convey critical information regarding your application,
////    as the notification area may not exist on the user's environment, or may have been removed.
////   You should always check that a status icon has been embedded into a notification area by using
////    gtk_status_icon_is_embedded(), and gracefully recover if the function returns FALSE.
////
////        THE DOCS ARE USELESS.
////        UNLESS CALLED FROM gtk_main(), the gtk_status_icon_is_embedded() fn  ALWAYS RETURNS FALSE
////
    gboolean trayok = gtk_status_icon_is_embedded(status_icon);
    gtk_status_icon_set_visible(status_icon, TRUE);
    if (!trayok) {
        GtkWidget* notray_dlg = gtk_message_dialog_new(NULL, GTK_DIALOG_MODAL,
            GTK_MESSAGE_OTHER, GTK_BUTTONS_OK, _("clipski requires a window manager tray \
[aka 'notification area'] for placement of its status icon. Unable to embed the \
status icon, so will now exit. "));
        gtk_window_set_title((GtkWindow*)notray_dlg, _("!"));
        //gtk_window_set_position(GTK_WINDOW(notray_dlg), GTK_WIN_POS_CENTER);
        if (gtk_dialog_run((GtkDialog*)notray_dlg) == GTK_RESPONSE_OK) {
            gtk_widget_destroy(notray_dlg);
        }
        exit(1);
    }
*/
}



#define APPLICATION_INSTANCE_MUTEX_NAME "{30042EB6-E62C-4F35-AC50-BF32FC3B16CB}"

int main(int argc, char **argv) {
	//    howdy  v-------- this, if performed THIS early, will nix our ability to accept stuffs piped from stdin?

	// ensure only a single instance of this program is running.
    // Using a UNIX domain socket with an ABSTRACT name, to serve as a mutex.
    // The bind() will fail if the socket is already bound by another owner,
    // and the O/S will cleanup the abstract socket when our process dies.
    int err, result;
    int mutex_sock = socket(AF_UNIX, SOCK_STREAM, 0);
    if (mutex_sock == -1) {
        printf("slipski failed creating mutex socket\n");
        exit(1);  //  howdy   begs better announcement, but this scenario should NEVER occur
    }
    // Bind to abstract socket. It will serve the role of a named mutex.
    char *oursocket_path = "\0clipski_socket";
    struct sockaddr_un addr;
    memset(&addr, 0, sizeof(addr));
    addr.sun_family = AF_UNIX;
    strncpy(addr.sun_path + 1, oursocket_path, sizeof(addr.sun_path) - 2);
    result = bind(mutex_sock, (struct sockaddr*) &addr, sizeof(addr));
    if (result == -1) {
/**
        err = errno;
        if (errno == EADDRINUSE) {
            printf("clipski failed bind to mutex socket. Another instance\n"
                    "with the same socket file appears to be already running.\n");
        } else {
            printf("failed bind to mutex socket. Will now exit.\n");
        }
*/         //   ^------- useful for debugging, but gibberish-speak from user POV
        printf("another instance of clipski is already running. Bye.\n");
        exit(1);
    }

    //// umask(0077);      // one of the forks would add this line
    gdk_set_allowed_backends("x11");  // howdy
	bindtextdomain(GETTEXT_PACKAGE, CLIPSKILOCALEDIR);
	bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
	textdomain(GETTEXT_PACKAGE);

	input_buffer[0] = '\0';
	input_index = 0;

	gtk_init(&argc, &argv);

	if (argc > 1) {    //  Parse options and exit if returns TRUE
		if (parse_options(argc, argv))
			return 0;
	} else {           //  Read input from stdin (if any)
		// Check if stdin is piped
		if (!isatty(fileno(stdin)))
		{
			GString* piped_string = g_string_new(NULL);
			/* Append stdin to string */
			while (1) {
				gchar* buffer = (gchar*)g_malloc(256);
				if (fgets(buffer, 256, stdin) == NULL) {
					g_free(buffer);
					break;
				}
				g_string_append(piped_string, buffer);
				g_free(buffer);
			}
			//  check if anything was piped in
			if (piped_string->len > 0)
			{	/* Truncate new line character */
				/* g_string_truncate(piped_string, (piped_string->len - 1)); */
				/* Copy to clipboard */
				GtkClipboard* clip = gtk_clipboard_get(GDK_SELECTION_CLIPBOARD);
				gtk_clipboard_set_text(clip, piped_string->str, -1);
				gtk_clipboard_store(clip);
				return 0;
			}
			g_string_free(piped_string, TRUE);
		}
	}

	clipski_init();
	init_history_timeout_timer();  // Create the history_timeout_timer if applicable

	gtk_main();   // run GTK+ loop

	/* Cleanup */
	g_list_foreach(history, (GFunc)g_free, NULL);
	g_list_free(history);
	g_free(primary_text);
	g_free(clipboard_text);
	g_free(synchronized_text);

	XCloseDisplay(display);
	return 0;
}
