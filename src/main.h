/* Copyright (C) 2010-2012 by Cristian Henzel <email@redact.ed>
 * Copyright (C) 2011 by Eugene Nikolsky <email@redact.ed>
 *
 * forked from parcellite, which is
 * Copyright (C) 2007-2008 by Xyhthyx <email@redact.ed>
 *
 * This file is part of clipski.
 *
 * clipski is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * clipski is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "preferences.h"

#ifndef MAIN_H
#define MAIN_H

G_BEGIN_DECLS

#define ACTIONS_TAB    2
#define POPUP_DELAY    250

#if GTK_MAJOR_VERSION == 3
#define GTK_DIALOG_NO_SEPARATOR 0
#endif

typedef struct {
  gboolean  use_copy;                 /* Use copy */
  gboolean  use_primary;              /* Use primary */
  gboolean  synchronize;              /* Synchronize copy and primary */
  gboolean  automatic_paste;          /* Automatically paste entry after selecting it  (howdy  currently only set true by manually editing rc file) */
  gboolean  show_indexes;             /* Show index numbers in history menu */
  gboolean  trim_whitespace;
  gboolean  save_uris;                /* Save URIs in history */
  gboolean  use_rmb_menu;             // Use Right-Mouse-Button Menu (avoids mainmenu clutter)

  gboolean  save_history;             /* Save history */
  gint      history_limit;            /* Items in history */
  gboolean  history_timeout;          /* Clear history after timeout */
  gint      history_timeout_seconds;  /* Seconds to wait before clearing history */

  gint      items_menu;               /* Items in history menu */
  gboolean  statics_show;             /* Show statics items in history menu */
  gint      statics_items;            /* Static items in history menu */

  gboolean  confirm_clear;            /* Confirm clear */
  gboolean  single_line;              /* Show in a single line */
  gboolean  reverse_history;          /* Show in reverse order */
  gint      item_length;              /* Length of items */
  gint      ellipsize;                /* Omitting */

  gboolean  hypersonic_potato;        // ignored, reserved for future use
  gboolean  standby_mode;             /* Standby mode */

  gchar*    exclude_windows;          /* Exclude clipboard content from windows, whose name matches this regexp */
} prefs_t;

extern prefs_t prefs;

void clear_main_data();


G_END_DECLS

#endif
