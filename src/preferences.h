/* Copyright (C) 2010-2012 by Cristian Henzel <email@redact.ed>
 * Copyright (C) 2011 by Eugene Nikolsky <email@redact.ed>
 *
 * forked from parcellite, which is
 * Copyright (C) 2007-2008 by Xyhthyx <email@redact.ed>
 *
 * This file is part of clipski.
 *
 * clipski is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * clipski is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PREFERENCES_H
#define PREFERENCES_H

G_BEGIN_DECLS

#define DEF_USE_COPY                  TRUE
#define DEF_USE_PRIMARY               FALSE
#define DEF_SYNCHRONIZE               FALSE
#define DEF_AUTOMATIC_PASTE           FALSE
#define DEF_SHOW_INDEXES              FALSE
#define DEF_TRIM_WHITESPACE           FALSE
#define DEF_SAVE_URIS                 TRUE
#define DEF_SAVE_HISTORY              TRUE
#define DEF_USE_RMB_MENU              FALSE
#define DEF_HISTORY_LIMIT             100
#define DEF_HISTORY_TIMEOUT           FALSE
#define DEF_HISTORY_TIMEOUT_SECONDS   30
#define DEF_ITEMS_MENU                23
#define DEF_STATICS_SHOW              TRUE
#define DEF_STATICS_ITEMS             15
#define DEF_CONFIRM_CLEAR             FALSE
#define DEF_SINGLE_LINE               TRUE
#define DEF_REVERSE_HISTORY           FALSE // howdy bub   winds up inverted first-run or when prefs file does not exist
#define DEF_ITEM_LENGTH               65
#define DEF_ELLIPSIZE                 2
#define DEF_HYPERSONIC_POTATO         FALSE       //  howdy   currently unused
#define DEF_STANDBY_MODE              FALSE
#define DEF_EXCLUDE_WINDOWS           NULL     //   howdy bub     compiler gripes "converting void to int"

#define ACTIONS_FILE          "clipski/actions"  // howdy  currently unused
#define EXCLUDES_FILE         "clipski/excludes"
#define PREFERENCES_FILE      "clipski/clipskirc"
#define THEMES_FOLDER         "clipski/themes"    //   howdy   nixme

#define SAVE_HIST_MESSAGE     "clipski can save your history in a plaintext file. If you copy \
passwords or other sensitive data and other people have access to the computer, this might be a \
security risk. Do you want to enable history saving?"

#define CHECK_HIST_MESSAGE    "It appears that you have disabled history saving, the current history \
file still exists though. Do you want to empty the current history file?\n, \
 consider this: EMPTYing will discard any static//pinned entries.\n\n \
In case your intent is to just temporarily SUSPEND adding new items, \
you can activate clipski's STANDBY mode instead of emptying."

// howdy      bear in mind whether (or not) to accommodate pango markup
#define EXCLUDETIPS_MESSAGE   "Regular expression to match against WINDOW NAME. For example\n \
           ^(keepass)|(lastpass)\nwill cause all windows whose name begins with \
either 'keepass' or 'lastpass' to be ignored by clipski.\n\n \
exclusion rules can also use simple substring matches.\n \n\n \
Each of these would, identically, prevent the snippet \n \
  'blahblah mypassword blah'\n \
from being stored in the history list:\n \
       mypass\n \
      *mypass*\n \
     .*mypass.*\n \
    ^.*mypass.*$\n\n \
note: The matching is case insensitive."

void read_preferences();

void save_preferences();

void show_preferences(gint tab);

void init_purge_timer();

G_END_DECLS

#endif
